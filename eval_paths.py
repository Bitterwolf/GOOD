import re

eval_paths = dict([]) #these should be lists of path strings of .pt files
eval_paths['MNIST'] = [
    #'experiments/GOOD_MNIST/06-30-13-20-49MGOODQ100e0.3k0.3ALoTINYac4/state_dicts/419fin.pt',
]

eval_paths['SVHN'] = [
]

eval_paths['CIFAR10'] = [
]


def get_arch(path):
    if 'AL' in path:
        assert'AX' not in path
        return 'L'
    elif 'AX' in path:
        return 'XL'
    else:
        raise ValueError('No valid arch substring in the path.')
        
def get_shortname(s):
    match = re.search(r'\d\d-\d\d-\d\d-\d\d-\d\d\D(.*)[ek]\d', s)
    return match.group(1)

def get_shortname(s):
    if 'GOODQ' in s:
        match = re.search(r'GOODQ\d+', s)
        name = match.group(0)
    elif 'CEDA' in s:
        name = 'CEDA'
    elif 'OE' in s:
        name = 'OE'
    elif 'plain' in s:
        name = 'Plain'
    else:
        return 'Unknown'
    if 'ACET' in s:
        if name == 'Plain':
            name = 'ACET' #+  s.split('acetce')[1].split('/')[0]
        else:
            name += 'ACET'
    return name